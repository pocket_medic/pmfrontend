(function () {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ngResource',
        'angular.filter',
        'ngMaterial',
        'ngMessages',
        'satellizer',
        //'templates',
        'app.config',
        'app.inicio',
        'app.footer',
        'app.header',
        'app.login',
        'app.ambiental',
        'app.registro',
        'app.portafolio',
        'app.perfil',
        'app.Usuarios',
        'app.configuracion',
        'sidenavDemo1',
        'app.tituloscertificados',
        'app.bienvenido'



    ]);

})();

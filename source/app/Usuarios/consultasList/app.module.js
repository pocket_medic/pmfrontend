(function () {
    'use strict';

    angular.module('app.consultasList', [
        'app.consultasList.services',
        'app.consultasList.route',
        'app.consultasList.controller',
        'app.consultasList.directivas'
    ]);

})();

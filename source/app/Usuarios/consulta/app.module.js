(function () {
    'use strict';

    angular.module('app.consulta', [
        'app.consulta.router',
        'app.consulta.directive',
        'app.consulta.controller'
    ]);

})();
